package ph.appsolutely.henann.content_activities.about;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.utilities.constants.UIConstants;

import static ph.appsolutely.henann.utilities.constants.UIConstants.ABOUT_LINK;
import static ph.appsolutely.henann.utilities.constants.UIConstants.NAVIGATION_LINKS;

/**
 * Created by Jibo on 1/30/18.
 */

public class AboutActivity extends AppCompatActivity {

    WebView webView;

    Toolbar toolbar;
    TextView toolbarTitle;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.content_activity_about);

        initialize();
    }

    private void initialize() {
        webView = findViewById(R.id.webview);
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.text_title);

        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        webView.loadUrl("file:///android_asset/html/about.html");

        setToolbar();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbarTitle.setText(NAVIGATION_LINKS[ABOUT_LINK]);

    }

}
