package ph.appsolutely.henann.content_activities.locations.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.content_activities.locations.pojo.LocationObj;

/**
 * Created by Jibo on 2/20/18.
 */

public class LocRecyclerAdapter extends RecyclerView.Adapter<LocRecyclerAdapter.ViewHolder> {

    private List<LocationObj> locations;

    public LocRecyclerAdapter(List<LocationObj> locations) {
        this.locations = locations;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView locName;
        public TextView locAddress;
        public LinearLayout listItem;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;

            listItem = v.findViewById(R.id.item);
            locName = v.findViewById(R.id.loc_name);
            locAddress = v.findViewById(R.id.loc_address);
        }
    }

    @Override
    public LocRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View v = inflater.inflate(R.layout.view_loc_section_item, parent, false);

        v.setLayoutParams(new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.locName.setText(locations.get(position).getName());
        holder.locAddress.setText(locations.get(position).getAddress());
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

}
