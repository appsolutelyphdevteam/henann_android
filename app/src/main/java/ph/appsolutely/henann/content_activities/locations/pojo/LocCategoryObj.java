package ph.appsolutely.henann.content_activities.locations.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jibo on 2/9/18.
 */

public class LocCategoryObj {


    @SerializedName("locCategoryID")
    @Expose
    private String locCategoryID;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;

    public String getLocCategoryID() {
        return locCategoryID;
    }

    public void setLocCategoryID(String locCategoryID) {
        this.locCategoryID = locCategoryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
