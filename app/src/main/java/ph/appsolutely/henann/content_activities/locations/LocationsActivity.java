package ph.appsolutely.henann.content_activities.locations;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.content_activities.locations.fragment.LocSectionFragment;
import ph.appsolutely.henann.content_activities.locations.pojo.LocCategoryObj;
import ph.appsolutely.henann.content_activities.locations.pojo.LocationObj;
import ph.appsolutely.henann.utilities.WebServices;
import ph.appsolutely.henann.utilities.LoaderDialog;

import static ph.appsolutely.henann.utilities.constants.ApiConst.FETCH_TABLE;
import static ph.appsolutely.henann.utilities.constants.ApiConst.LOC;
import static ph.appsolutely.henann.utilities.constants.ErrorMessages.CONNECTION_ERR_MSG;
import static ph.appsolutely.henann.utilities.constants.ErrorMessages.JSON_PARSE_ERR_MSG;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_TITLE;
import static ph.appsolutely.henann.utilities.constants.KeyConst.DESCRIPTION;
import static ph.appsolutely.henann.utilities.constants.UIConstants.LOCATION_LINK;
import static ph.appsolutely.henann.utilities.constants.UIConstants.NAVIGATION_LINKS;

/**
 * Created by Jibo on 1/30/18.
 */

public class LocationsActivity
        extends AppCompatActivity
        implements
            Response.Listener<String>,
            Response.ErrorListener {

    private static final String FRAG_TAG_PREFIX     = "section_";
    private static final String LOC_CATEGORY_KEY    = "locCategory";
    public static final String LOC_KEY              = "location";

    Toolbar toolbar;
    TextView toolbarTitle;

    LinearLayout sectionWrapper;

    LoaderDialog loader;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.content_activity_locations);

        initialize();
    }

    private void initialize() {

        sectionWrapper = findViewById(R.id.section_wrapper);

        loader = new LoaderDialog(this);

        setToolbar();

        WebServices.fetchJson(FETCH_TABLE[LOC], this);
        loader.show();

    }

    private void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.text_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbarTitle.setText(NAVIGATION_LINKS[LOCATION_LINK]);
    }

    private void setLocations(LocCategoryObj[] categories, LocationObj[] locations) {

        int sec_count = 0;

        for (LocCategoryObj category : categories) {

            // filter locations
            ArrayList<Parcelable> parc_loc = new ArrayList<>();

            for (LocationObj location : locations) {
                if (category.getLocCategoryID().equals(location.getLocCategoryID())) {
                    parc_loc.add(location);
                }
            }

            // set fragment args
            Bundle args = new Bundle();

            args.putString(IE_TITLE, category.getName());
            args.putParcelableArrayList(LOC_KEY, parc_loc);

            // set fragment
            Fragment fragment = new LocSectionFragment();

            fragment.setArguments(args);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.section_wrapper, fragment, FRAG_TAG_PREFIX + sec_count)
                    .commit();

            sec_count++;

        }

    }

    @Override
    public void onResponse(String response) {
        loader.hide();
        try {
            JSONObject jsonResponse = new JSONObject(response);
            
            if (jsonResponse.getString("response").equals("Success")) {
                JSONArray jsonData = jsonResponse.getJSONArray("data");

                if (jsonData.length() != 0) {
                    JSONArray jsonSection = jsonData.getJSONObject(0).getJSONArray(LOC_CATEGORY_KEY);
                    JSONArray jsonLocation = jsonData.getJSONObject(0).getJSONArray(LOC_KEY);

                    LocCategoryObj[] categories = new Gson().fromJson(jsonSection.toString(), LocCategoryObj[].class);
                    LocationObj[] locations = new Gson().fromJson(jsonLocation.toString(), LocationObj[].class);

                    setLocations(categories, locations);

                } else {
                    // TODO: add empty state
                }

            } else {
                Toast.makeText(this, jsonResponse.getString(DESCRIPTION), Toast.LENGTH_SHORT).show();
            }

        } catch(JSONException e) {
            Toast.makeText(this, JSON_PARSE_ERR_MSG, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        loader.hide();
        Toast.makeText(this, CONNECTION_ERR_MSG, Toast.LENGTH_SHORT).show();
        error.printStackTrace();
    }

}
