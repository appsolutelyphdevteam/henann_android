package ph.appsolutely.henann.content_activities.locations.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.content_activities.locations.adapter.LocRecyclerAdapter;
import ph.appsolutely.henann.content_activities.locations.pojo.LocationObj;

import static ph.appsolutely.henann.content_activities.locations.LocationsActivity.LOC_KEY;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_TITLE;

/**
 * Created by Jibo on 2/19/18.
 */

public class LocSectionFragment extends Fragment {

    TextView sectionTitle;


    RecyclerView recyclerView;
    RecyclerView.Adapter recyclerAdapter;
    RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.view_loc_section, container, false);

        initialize(view);

        return view;
    }

    private void initialize(View v) {
        sectionTitle = v.findViewById(R.id.section_title);
        recyclerView = v.findViewById(R.id.list);

        List<LocationObj> locationData = getArguments().getParcelableArrayList(LOC_KEY);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerAdapter = new LocRecyclerAdapter(locationData);

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);

        sectionTitle.setText(getArguments().getString(IE_TITLE));

    }

}
