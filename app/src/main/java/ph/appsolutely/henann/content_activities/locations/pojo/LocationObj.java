package ph.appsolutely.henann.content_activities.locations.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jibo on 2/9/18.
 */

public class LocationObj implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("locID")
    @Expose
    private String locID;
    @SerializedName("locCategoryID")
    @Expose
    private String locCategoryID;
    @SerializedName("brandID")
    @Expose
    private String brandID;
    @SerializedName("brandName")
    @Expose
    private String brandName;
    @SerializedName("subbrandID")
    @Expose
    private String subbrandID;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("businessHrs")
    @Expose
    private String businessHrs;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("branchCode")
    @Expose
    private String branchCode;
    @SerializedName("order")
    @Expose
    private String order;
    @SerializedName("locFlag")
    @Expose
    private String locFlag;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("dateAdded")
    @Expose
    private String dateAdded;
    @SerializedName("dateModified")
    @Expose
    private String dateModified;
    @SerializedName("locCategoryName")
    @Expose
    private String locCategoryName;

    public LocationObj() {}

    // parcelable
    public LocationObj(Parcel in){
        String[] data = new String[22];

        in.readStringArray(data);

        this.id                 = data[0];
        this.locID              = data[1];
        this.locCategoryID      = data[2];
        this.brandID            = data[3];
        this.brandName          = data[4];
        this.subbrandID         = data[5];
        this.image              = data[6];
        this.address            = data[7];
        this.latitude           = data[8];
        this.longitude          = data[9];
        this.name               = data[10];
        this.phone              = data[11];
        this.email              = data[12];
        this.businessHrs        = data[13];
        this.region             = data[14];
        this.branchCode         = data[15];
        this.order              = data[16];
        this.locFlag            = data[17];
        this.status             = data[18];
        this.dateAdded          = data[19];
        this.dateModified       = data[20];
        this.locCategoryName    = data[21];
    }

    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {
                this.id,
                this.locID,
                this.locCategoryID,
                this.brandID,
                this.brandName,
                this.subbrandID,
                this.image,
                this.address,
                this.latitude,
                this.longitude,
                this.name,
                this.phone,
                this.email,
                this.businessHrs,
                this.region,
                this.branchCode ,
                this.order,
                this.locFlag,
                this.status,
                this.dateAdded,
                this.dateModified,
                this.locCategoryName
        });
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public LocationObj createFromParcel(Parcel in) {
            return new LocationObj(in);
        }

        public LocationObj[] newArray(int size) {
            return new LocationObj[size];
        }
    };


    // getters setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocID() {
        return locID;
    }

    public void setLocID(String locID) {
        this.locID = locID;
    }

    public String getLocCategoryID() {
        return locCategoryID;
    }

    public void setLocCategoryID(String locCategoryID) {
        this.locCategoryID = locCategoryID;
    }

    public String getBrandID() {
        return brandID;
    }

    public void setBrandID(String brandID) {
        this.brandID = brandID;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getSubbrandID() {
        return subbrandID;
    }

    public void setSubbrandID(String subbrandID) {
        this.subbrandID = subbrandID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBusinessHrs() {
        return businessHrs;
    }

    public void setBusinessHrs(String businessHrs) {
        this.businessHrs = businessHrs;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getLocFlag() {
        return locFlag;
    }

    public void setLocFlag(String locFlag) {
        this.locFlag = locFlag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getLocCategoryName() {
        return locCategoryName;
    }

    public void setLocCategoryName(String locCategoryName) {
        this.locCategoryName = locCategoryName;
    }
}
