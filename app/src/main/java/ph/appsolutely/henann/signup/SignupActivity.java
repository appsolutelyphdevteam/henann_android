package ph.appsolutely.henann.signup;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.utilities.WebServices;
import ph.appsolutely.henann.utilities.input.Input;
import ph.appsolutely.henann.utilities.LoaderDialog;

import static ph.appsolutely.henann.utilities.constants.ErrorMessages.INVALID_EMAIL;
import static ph.appsolutely.henann.utilities.constants.ErrorMessages.PASSWORD_FORMAT;
import static ph.appsolutely.henann.utilities.constants.KeyConst.DOB;
import static ph.appsolutely.henann.utilities.constants.KeyConst.EMAIL;
import static ph.appsolutely.henann.utilities.constants.KeyConst.FNAME;
import static ph.appsolutely.henann.utilities.constants.KeyConst.LNAME;
import static ph.appsolutely.henann.utilities.constants.KeyConst.PASSWORD;

/**
 * Created by Jibo on 1/22/18.
 */

public class SignupActivity
        extends AppCompatActivity
        implements
            Response.Listener<String>,
            Response.ErrorListener {

    Toolbar toolbar;

    Input email;
    Input password;
    Input fname;
    Input lname;
    Input dob;

    AppCompatEditText dobInput;

    Calendar mCalendar;

    LinearLayout signupBtn;

    LoaderDialog loader;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_signup);

        initialize();
    }

    private void initialize() {

        toolbar         = findViewById(R.id.toolbar);
        signupBtn       = findViewById(R.id.btn_signup);

        loader          = new LoaderDialog(this);

        setToolbar();
        setInputValidation();
        setDatePicker();

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setBackgroundColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setInputValidation() {
        email       = new Input((TextInputLayout) findViewById(R.id.wrapper_email), Input.Type.EMAIL, true);
        password    = new Input((TextInputLayout) findViewById(R.id.wrapper_password), Input.Type.PASSWORD, true);
        fname       = new Input((TextInputLayout) findViewById(R.id.wrapper_fname), Input.Type.TEXT, true);
        lname       = new Input((TextInputLayout) findViewById(R.id.wrapper_lname), Input.Type.TEXT, true);
        dob         = new Input((TextInputLayout) findViewById(R.id.wrapper_dob), Input.Type.DOB, true);
        dobInput    = (AppCompatEditText) dob.getEditText();

        email.setErrorMsg(INVALID_EMAIL);
        password.setErrorMsg(PASSWORD_FORMAT);
        password.setHelperTxt(PASSWORD_FORMAT);

    }

    private void setDatePicker() {
        final String mFormat = "MMMM dd, yyyy";

        mCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, monthOfYear);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat(mFormat, Locale.US);

                dobInput.setText(sdf.format(mCalendar.getTime()));

            }

        };

        dobInput.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                DatePickerDialog dialog = new DatePickerDialog(SignupActivity.this, date, mCalendar
                        .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH));

                dialog.getDatePicker().setMaxDate(new Date().getTime());
                dialog.show();
            }
        });

        // disable input copy paste
        dobInput.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode actionMode, MenuItem item) {
                return false;
            }

            public void onDestroyActionMode(ActionMode actionMode) {
            }
        });
        dobInput.setTextIsSelectable(false);

    }

    private void signup() {

        email.check();
        password.check();
        fname.check();
        lname.check();
        dob.check();

        if (email.isValid() && password.isValid()
                && fname.isValid() && lname.isValid() && dob.isValid()) {

            Map<String, String> params = new HashMap<>();

            params.put(EMAIL,       email.getVal());
            params.put(FNAME,       fname.getVal());
            params.put(LNAME,       lname.getVal());
            params.put(PASSWORD,    password.getVal());
            params.put(DOB,         dob.getVal());

            WebServices.signUpRequest(this, params);
            loader.show();

        }

    }

    @Override
    public void onResponse(String response) {
        loader.hide();

        Log.i("tag",response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        loader.hide();
        error.printStackTrace();
    }


}
