package ph.appsolutely.henann;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ph.appsolutely.henann.dashboard.DashboardActivity;
import ph.appsolutely.henann.login.LoginActivity;
import ph.appsolutely.henann.utilities.PrefManager;
import ph.appsolutely.henann.walkthrough.WalkthroughActivity;

public class SplashActivity extends AppCompatActivity {

    private static final int DEFAULT_DELAY_MS = 3000;

    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

//                if (prefManager.isFirstLaunch()) {
//                    startActivity(new Intent(SplashActivity.this, WalkthroughActivity.class));
//                } else {
                    if (!prefManager.isLoggedIn()) {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                    }
//                }

                finish();

            }
        }, DEFAULT_DELAY_MS);

    }
}
