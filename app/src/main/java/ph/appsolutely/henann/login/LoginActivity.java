package ph.appsolutely.henann.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.signup.SignupActivity;


/**
 * Created by Jibo on 1/22/18.
 */

public class LoginActivity extends AppCompatActivity {

    TextView signupLink;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_login);

        initialize();

    }

    private void initialize() {
        signupLink = findViewById(R.id.link_signup);

        signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            }
        });
    }
}
