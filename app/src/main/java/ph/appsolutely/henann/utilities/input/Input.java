package ph.appsolutely.henann.utilities.input;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import java.util.regex.Pattern;

import ph.appsolutely.henann.R;

/**
 * Created by Jibo on 2/21/18.
 */

public class Input {

    private TextInputLayout wrapper;
    private EditText editText;
    private int inputType;
    private boolean isRequired;

    private String ERROR_MSG    = "";
    private String HELPER_TXT   = "";

    // defaults
    private static final int PASSWORD_LENGTH = 8;
    private static final String ALPHA_NUM_REGEX = "^[a-zA-Z0-9]*$";

    public class Type {
        public static final int EMAIL        = 0;
        public static final int PASSWORD     = 1;
        public static final int TEXT         = 2;
        public static final int MOBILE       = 3;
        public static final int DOB          = 4;
    }

    public Input(TextInputLayout wrapper, int inputType, boolean isRequired) {
        this.wrapper    = wrapper;
        this.editText   = wrapper.getEditText();
        this.inputType  = inputType;
        this.isRequired = isRequired;

        initialize();
    }

    private void initialize() {

        wrapper.setErrorEnabled(true);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                validate(s.toString());
            }
        });

    }

    public void setErrorMsg(String error) {
        this.ERROR_MSG = error;
    }

    public void setHelperTxt(String helper) {
        this.HELPER_TXT = helper;

        wrapper.setError(helper);
        wrapper.setHintEnabled(false);
        wrapper.setErrorTextAppearance(R.style.TextInputHelper);

        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    wrapper.setHintEnabled(true);
                } else {
                    if (editText.getText().toString().isEmpty())
                        wrapper.setHintEnabled(false);
                }
            }
        });
    }

    private void showError() {
        wrapper.setError(ERROR_MSG);
        wrapper.setErrorTextAppearance(R.style.TextInputError);
    }

    private void showError(String error) {
        wrapper.setError(error);
        wrapper.setErrorTextAppearance(R.style.TextInputError);
    }

    private void hideError() {
        if (HELPER_TXT.isEmpty()) {
            wrapper.setError(null);
        } else {
            wrapper.setError(HELPER_TXT);
            wrapper.setErrorTextAppearance(R.style.TextInputHelper);
        }

    }

    public void check() { validate(editText.getText().toString()); }

    public boolean isValid() {
        return validate(editText.getText().toString());
    }

    private boolean validate(String s) {

        if (isRequired && s.isEmpty()) {
            showError("Required");
            return false;
        } else {
            hideError();
        }

        switch (inputType) {
            case Type.EMAIL:        return validateEmail(s);
            case Type.PASSWORD:     return validatePassword(s);
            case Type.TEXT:         return validateText(s);
            case Type.MOBILE:       return validateMobile(s);
            case Type.DOB:          return validateDob(s);
            default:                return false;
        }
    }

    public String getVal() {
        return editText.getText().toString();
    }

    public EditText getEditText() {
        return wrapper.getEditText();
    }

    private boolean validateEmail(String input) {
        if (!TextUtils.isEmpty(input)
                && Patterns.EMAIL_ADDRESS.matcher(input).matches()) {
            hideError();
            return true;
        }

        showError();
        return false;
    }

    private boolean validatePassword(String input) {
        if (!input.isEmpty() && input.length() >= PASSWORD_LENGTH
                && Pattern.compile(ALPHA_NUM_REGEX).matcher(input).matches()) {
            hideError();
            return true;
        }
        showError();
        return false;
    }

    private boolean validateText(String input) {
        return true;
    }

    private boolean validateMobile(String input) {
        return true;
    }

    private boolean validateDob(String input) {
        return true;
    }




}
