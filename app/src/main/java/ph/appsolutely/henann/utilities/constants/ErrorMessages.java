package ph.appsolutely.henann.utilities.constants;

/**
 * Created by Jibo on 2/19/18.
 */

public class ErrorMessages {

    public static final String CONNECTION_ERR_MSG = "Cannot establish a connection.";
    public static final String JSON_PARSE_ERR_MSG = "Something went wrong.";

    // input
    public static final String INPUT_REQUIRED   = "Required";
    public static final String INVALID_EMAIL    = "Email is invalid";
    public static final String PASSWORD_FORMAT  = "Password must contain at least eight (8) alphanumeric characters";


}
