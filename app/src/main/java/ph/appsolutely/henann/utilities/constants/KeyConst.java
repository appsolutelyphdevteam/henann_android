package ph.appsolutely.henann.utilities.constants;

/**
 * Created by Jibo on 2/6/18.
 */

public class KeyConst {

    /**
     * COMMON
     *
     */

    // user
    public static final String EMAIL        = "email";
    public static final String PASSWORD     = "password";
    public static final String FNAME        = "fname";
    public static final String LNAME        = "lname";
    public static final String DOB          = "dateOfBirth";
    public static final String MOBILE_NUM   = "mobileNum";
    public static final String GENDER       = "gender";

    // content
    public static final String DESCRIPTION  = "description";

    /**
     * INTENT KEYS
     *
     */

    public static final String IE_TITLE = "content_title";
    public static final String IE_IMG_URL = "content_img_url";
    public static final String IE_DESCRIPTION = "content_desc";


}
