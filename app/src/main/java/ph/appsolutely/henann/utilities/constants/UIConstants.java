package ph.appsolutely.henann.utilities.constants;

/**
 * Created by Jibo on 1/30/18.
 */

public class UIConstants {

    /**
     * PAGES
     */

    public static final int NEWS_PAGE       = 0;
    public static final int REWARDS_PAGE    = 1;
    public static final int CODE_PAGE       = 2;
    public static final int VOUCHERS_PAGE   = 3;
    public static final int OFFERS_PAGE     = 4;

    public static final String[] TOOLBAR_TITLE = { "What\'s new", "Rewards", "My code", "Vouchers", "Offers" };

    /**
     * DRAWER LINKS
     *
     */

    public static final int LOCATION_LINK   = 0;
    public static final int ABOUT_LINK      = 1;
    public static final int FAQS_LINK       = 2;
    public static final int TERMS_LINK      = 3;
    public static final int LOGOUT_LINK     = 4;

    public static final String[] NAVIGATION_LINKS = { "Locations", "About us", "FAQs", "Terms & Conditions", "Log out" };
}
