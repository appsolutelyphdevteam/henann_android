package ph.appsolutely.henann.utilities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;

import dmax.dialog.SpotsDialog;
import ph.appsolutely.henann.R;

/**
 * Created by Jibo on 1/22/18.
 */

public class LoaderDialog {
    AlertDialog alertDialog;
    ProgressDialog progressDialog;

    private static final String DEFAULT_DIALOG_LABEL = "Loading...";

    private static final int version = Build.VERSION.SDK_INT;
    private Context mContext;

    public LoaderDialog(Context mContext) {
        this.mContext = mContext;

        this.alertDialog = new SpotsDialog(mContext, R.style.CustomSpotsDialog);
        this.alertDialog.setCancelable(false);

        this.progressDialog = new ProgressDialog(mContext);
        this.progressDialog.setCancelable(false);
        this.progressDialog.setMessage(DEFAULT_DIALOG_LABEL);
    }

    public void setMessage(String label) {
        if ( version >= Build.VERSION_CODES.O ) {
            if (alertDialog != null)
                alertDialog.setMessage(label);
        } else {
            if (progressDialog != null)
                progressDialog.setMessage(label);
        }
    }

    public void show() {
        if ( version >= Build.VERSION_CODES.O ) {
            if (alertDialog != null)
                alertDialog.show();
        } else {
            if (progressDialog != null)
                progressDialog.show();
        }
    }

    public void hide() {
        if (version >= Build.VERSION_CODES.O) {
            if (alertDialog != null)
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
        } else {
            if (progressDialog != null)
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
        }
    }


}
