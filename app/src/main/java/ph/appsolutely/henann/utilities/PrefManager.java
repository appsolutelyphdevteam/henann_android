package ph.appsolutely.henann.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Jibo on 1/22/18.
 */

public class PrefManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context mContext;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "app-henan";

    // strings
    private static final String IS_FIRST_LAUNCH = "first_launch";
    private static final String IS_LOGGED_IN = "logged_in";

    public PrefManager(Context mContext) {
        this.mContext = mContext;
        pref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    // FIRST LAUNCH
    public void setFirstLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstLaunch() {
        return pref.getBoolean(IS_FIRST_LAUNCH, true);
    }

    // LOGGED IN
    public void setIsLoggedIn(boolean loggedIn) {
        editor.putBoolean(IS_LOGGED_IN, loggedIn);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGGED_IN, false);
    }



}
