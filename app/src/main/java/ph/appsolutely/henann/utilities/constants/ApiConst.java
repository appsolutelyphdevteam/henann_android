package ph.appsolutely.henann.utilities.constants;

/**
 * Created by Jibo on 2/8/18.
 */

public class ApiConst {

    public static final String BASE_URL             = "http://54.254.157.211/";
    public static final String API_URL              = BASE_URL + "api.php";
    public static final String UPLOAD_URL           = BASE_URL + "upload.php";


    /**
     * PARAMS
     *
     */

    public static final String CATEGORY     = "category";
    public static final String FUNCTION     = "function";
    public static final String TABLE        = "table";
    public static final String TOKEN        = "token";
    public static final String OAUTH        = "oauth";


    /**
     * CATEGORIES
     *
     */

    public static final int JSON    = 0;
    public static final int CORE    = 1;

    public static final String[] API_CATEGORY = { "json", "core" };


    /**
     * FUNCTIONS
     *
     */

    public static final int FETCH             = 0;
    public static final int REGISTRATION      = 1;

    public static final String[] API_FUNCTION = { "fetch", "temp_registration" };


    /**
     * JSON FETCH TABLES
     *
     */

    public static final int LOC       = 0;
    public static final int POST      = 1;
    public static final int PRODUCT   = 2;

    public static final String[] FETCH_TABLE = { "loctable", "posttable", "producttable" };

}
