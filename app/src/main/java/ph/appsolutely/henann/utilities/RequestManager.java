package ph.appsolutely.henann.utilities;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Jibo on 2/8/18.
 */

public class RequestManager {
    private static final String LOG_TAG = "RequestManager";
    private static final String GET_MSG_CALL_FROM = "Call: from() before calling withParams()";
    private static final String MSG_URL_REQUIRED = "Url is required! Call: from()";
    private static final String MSG_LISTENERS_REQUIRED = "Response and Error Listener required! Call: doOnSuccess() / doOnError()";

    public static class GetRequest {

        private Context _ctx;
        private String _url;
        private Response.Listener<String> _onSuccess;
        private Response.ErrorListener _onError;

        public GetRequest(Context context) {
            this._ctx = context;
        }

        public GetRequest from(String url) {
            this._url = url;
            return GetRequest.this;
        }

        public GetRequest withParams(HashMap<String, String> params) {
            if (!_url.isEmpty()) {

                StringBuilder url = new StringBuilder(_url);
                Iterator it = params.entrySet().iterator();
                int count = 0;

                while (it.hasNext()) {
                    HashMap.Entry pair = (HashMap.Entry) it.next();

                    if (count == 0) url.append("?");

                    url.append(pair.getKey());
                    url.append("=");
                    url.append(pair.getValue());

                    if (it.hasNext()) url.append("&");

                    count++;

                    it.remove();
                }

                _url = url.toString();

                return GetRequest.this;

            } else {
                Log.e(LOG_TAG, GET_MSG_CALL_FROM);
                return null;
            }

        }

        public GetRequest doOnSuccess(Response.Listener<String> responseListener) {
            this._onSuccess = responseListener;
            return GetRequest.this;
        }

        public GetRequest doOnError(Response.ErrorListener errorListener) {
            this._onError = errorListener;
            return GetRequest.this;
        }

        public void startRequest() {
            if (_url.isEmpty()) {
                Log.e(LOG_TAG, MSG_URL_REQUIRED);
                return;
            }

            if (_onSuccess != null && _onError != null) {

                StringRequest request = new StringRequest(Request.Method.GET, _url, _onSuccess, _onError);

                RequestQueue requestQueue = Volley.newRequestQueue(_ctx);

                requestQueue.add(request);
            } else {
                Log.e(LOG_TAG, MSG_LISTENERS_REQUIRED);
            }
        }


    }

    public static class PostRequest {

        private Context _ctx;
        private String _url;
        private Map<String, String> _params;
        private Response.Listener<String> _onSuccess;
        private Response.ErrorListener _onError;

        public PostRequest(Context context) {
            this._ctx = context;
        }

        public PostRequest from(String url) {
            this._url = url;
            return PostRequest.this;
        }

        public PostRequest withParams(Map<String, String> params) {
            this._params = params;
            return PostRequest.this;
        }

        public PostRequest doOnSuccess(Response.Listener<String> responseListener) {
            this._onSuccess = responseListener;
            return PostRequest.this;
        }

        public PostRequest doOnError(Response.ErrorListener errorListener) {
            this._onError = errorListener;
            return PostRequest.this;
        }

        public void startRequest() {
            if (_url.isEmpty()) {
                Log.e(LOG_TAG, MSG_URL_REQUIRED);
                return;
            }

            if (_onSuccess != null && _onError != null) {

                StringRequest request = new StringRequest(Request.Method.POST, _url, _onSuccess, _onError) {

                    @Override
                    protected Map<String,String> getParams(){
                        return _params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(_ctx);

                requestQueue.add(request);

            } else {
                Log.e(LOG_TAG, MSG_LISTENERS_REQUIRED);
            }
        }
    }
}
