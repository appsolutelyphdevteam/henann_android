package ph.appsolutely.henann.utilities;

import android.app.Activity;
import android.content.Context;

import com.android.volley.Response;

import java.util.HashMap;
import java.util.Map;

import ph.appsolutely.henann.utilities.libs.CryptLib;

import static ph.appsolutely.henann.utilities.constants.ApiConst.API_CATEGORY;
import static ph.appsolutely.henann.utilities.constants.ApiConst.API_FUNCTION;
import static ph.appsolutely.henann.utilities.constants.ApiConst.API_URL;
import static ph.appsolutely.henann.utilities.constants.ApiConst.CATEGORY;
import static ph.appsolutely.henann.utilities.constants.ApiConst.CORE;
import static ph.appsolutely.henann.utilities.constants.ApiConst.FETCH;
import static ph.appsolutely.henann.utilities.constants.ApiConst.FUNCTION;
import static ph.appsolutely.henann.utilities.constants.ApiConst.JSON;
import static ph.appsolutely.henann.utilities.constants.ApiConst.OAUTH;
import static ph.appsolutely.henann.utilities.constants.ApiConst.REGISTRATION;
import static ph.appsolutely.henann.utilities.constants.ApiConst.TABLE;
import static ph.appsolutely.henann.utilities.constants.ApiConst.TOKEN;

/**
 * Created by Jibo on 2/8/18.
 */

public class WebServices {

    /**
     * FOR ENCRYPTION
     *
     * @return Object[] {
     *      CryptLib instance,
     *      random generated key,
     *      SHA256 key for OAUTH,
     *      iv for IV
     *  }
     */

    private static CryptLib getCryptLibInstance() {
        try {
            return new CryptLib();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String generateIV() {
        return CryptLib.generateRandomIV(16);
    }

    private static String getOAuth(String randomKey) {
        try {
            return CryptLib.SHA256(randomKey,32);
        } catch (Exception e) {
            return "";
        }
    }


    /**
     *
     * FETCH JSON
     *
     * request_type: GET
     * request_params:
     *      - category  : 'json'
     *      - function  : 'fetch'
     *      - table     : tableName
     *
     * method_params:
     *      - Context context   : required for RequestManager
     *      - String table      : json content table to fetch
     *      - Object listeners  : Volley Response.Listener[String] and Response.ErrorListener
     *      - Response.Listener[String] listener
     *      - Response.ErrorListener errorListener
     *
     */

    public static void fetchJson(String table, Activity activity) {
        fetchJsonRequest(activity, table, (Response.Listener<String>) activity, (Response.ErrorListener) activity);
    }

    public static void fetchJson(Context context, String table, Object listeners) {
        fetchJsonRequest(context, table, (Response.Listener<String>) listeners, (Response.ErrorListener) listeners);
    }

    public static void fetchJson(Context context, String table, Response.Listener<String> listener,
                                 Response.ErrorListener errorListener) {
        fetchJsonRequest(context, table, listener, errorListener);
    }

    private static void fetchJsonRequest(Context context, String table, Response.Listener<String> listener,
                                  Response.ErrorListener errorListener) {

        HashMap<String, String> params = new HashMap<>();

        params.put(CATEGORY, API_CATEGORY[JSON]);
        params.put(FUNCTION, API_FUNCTION[FETCH]);
        params.put(TABLE, table);

        new RequestManager
                .GetRequest(context)
                .from(API_URL)
                .withParams(params)
                .doOnSuccess(listener)
                .doOnError(errorListener)
                .startRequest();

    }


    /**
     *
     * SIGN UP
     *
     * request_type: POST
     *
     */

    public static void signUpRequest(Activity activity, Map<String, String> params) {

        // encrypt get and post params
        try {

            CryptLib crypt     = getCryptLibInstance();
            String randomKey   = generateIV();
            String key         = getOAuth(randomKey);
            String iv          = generateIV();

            String url = API_URL +
                    "?" + CATEGORY + "=" + crypt.encrypt(API_CATEGORY[CORE], key, iv)[0] +
                    "&" + FUNCTION + "=" + crypt.encrypt(API_FUNCTION[REGISTRATION], key, iv)[0];

            Map<String, String> enc_params = new HashMap<>();

            for (Map.Entry<String, String> param : params.entrySet()) {
                enc_params.put(param.getKey(), crypt.encrypt(param.getValue(), key, iv)[0]);
            }

            enc_params.put(OAUTH, key);
            enc_params.put(TOKEN, iv);

            // build and start request
            new RequestManager
                    .PostRequest(activity)
                    .from(url)
                    .withParams(enc_params)
                    .doOnSuccess((Response.Listener) activity)
                    .doOnError((Response.ErrorListener) activity)
                    .startRequest();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
