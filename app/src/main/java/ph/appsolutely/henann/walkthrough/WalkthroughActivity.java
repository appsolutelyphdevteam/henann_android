package ph.appsolutely.henann.walkthrough;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ph.appsolutely.henann.R;

/**
 * Created by Jibo on 1/22/18.
 */

public class WalkthroughActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_walkthrough);

        initialize();

    }

    private void initialize() {

    }
}
