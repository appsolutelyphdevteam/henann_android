package ph.appsolutely.henann.dashboard.content.offers.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jibo on 2/1/18.
 */

public class OffersObj implements Parcelable {

    @SerializedName("brandID")
    @Expose
    private String brandID;
    @SerializedName("prodID")
    @Expose
    private String prodID;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("dateAdded")
    @Expose
    private String dateAdded;

    public OffersObj() {}

    // parcelable
    public OffersObj(Parcel in){
        String[] data = new String[7];

        in.readStringArray(data);

        this.brandID =      data[0];
        this.name =         data[1];
        this.description =  data[2];
        this.price =        data[3];
        this.image =        data[4];
        this.status =       data[5];
        this.dateAdded =    data[6];
    }

    public int describeContents(){
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {
            this.brandID,
            this.name,
            this.description,
            this.price,
            this.image,
            this.status,
            this.dateAdded
        });
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public OffersObj createFromParcel(Parcel in) {
            return new OffersObj(in);
        }

        public OffersObj[] newArray(int size) {
            return new OffersObj[size];
        }
    };

    // getters setters
    public String getBrandID() {
        return brandID;
    }

    public void setBrandID(String brandID) {
        this.brandID = brandID;
    }

    public String getProdID() {
        return prodID;
    }

    public void setProdID(String prodID) {
        this.prodID = prodID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }
}
