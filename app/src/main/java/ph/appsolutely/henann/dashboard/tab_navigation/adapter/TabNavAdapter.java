package ph.appsolutely.henann.dashboard.tab_navigation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.tab_navigation.pojo.TabLink;

/**
 * Created by Jibo on 1/29/18.
 */

public class TabNavAdapter extends RecyclerView.Adapter<TabNavAdapter.ViewHolder> {

    TabLink[] tabs;
    Context mContext;

    View _tab;

    int _prevPosition = -1;

    public TabNavAdapter(Context mContext, TabLink[] tabs){
        this.tabs = tabs;
        this.mContext = mContext;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tabText;
        public ImageView tabImg;

        public ViewHolder(View v){

            super(v);

            tabText = v.findViewById(R.id.tab_text);
            tabImg = v.findViewById(R.id.tab_img);
        }
    }

    @Override
    public TabNavAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        _tab = LayoutInflater.from(mContext).inflate(R.layout.view_tab_bar_item, parent,false);

        int tab_width = parent.getMeasuredWidth() / getItemCount();

        _tab.setLayoutParams(new RecyclerView.LayoutParams(tab_width, ViewGroup.LayoutParams.MATCH_PARENT));

        return new ViewHolder(_tab);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tabText.setText(tabs[position].getTabTitle());

        if (tabs[position].getIsActive()) {
            holder.tabText.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            holder.tabImg.setImageResource(tabs[position].getImgResourceActive());
        } else {
            holder.tabText.setTextColor(mContext.getResources().getColor(R.color.mediumGray));
            holder.tabImg.setImageResource(tabs[position].getImgResource());
        }
    }

    @Override
    public int getItemCount() {
        return tabs.length;
    }

    public void setActiveTab(int position) {
        if (position != _prevPosition) {
            tabs[position].setIsActive(true);

            if (_prevPosition != -1) {
                tabs[_prevPosition].setIsActive(false);
            }

            _prevPosition = position;

            notifyDataSetChanged();
        }
    }

    public int getPosition() {
        return _prevPosition;
    }


}
