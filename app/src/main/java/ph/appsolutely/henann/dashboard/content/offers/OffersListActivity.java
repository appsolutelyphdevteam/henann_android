package ph.appsolutely.henann.dashboard.content.offers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.content.offers.adapter.OffersListAdapter;
import ph.appsolutely.henann.dashboard.content.offers.pojo.OffersObj;

import static ph.appsolutely.henann.dashboard.content.offers.OffersPage.PARC_OFFERS_KEY;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_DESCRIPTION;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_IMG_URL;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_TITLE;

/**
 * Created by Jibo on 2/6/18.
 */

public class OffersListActivity extends AppCompatActivity {

    Bundle args;

    Toolbar toolbar;
    TextView toolbarTitle;

    ListView offersList;
    OffersListAdapter offersListAdapter;


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.content_activity_offers_list);

        initialize();
    }

    private void initialize() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.text_title);
        offersList = findViewById(R.id.list);

        args = getIntent().getExtras();

        setToolbar();
        setOffersList();

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbarTitle.setText(args.getString(IE_TITLE));
    }

    private void setOffersList() {
        final List<OffersObj> offers = args.getParcelableArrayList(PARC_OFFERS_KEY);

        offersListAdapter = new OffersListAdapter(this, offers);
        offersList.setAdapter(offersListAdapter);
        offersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(OffersListActivity.this, OfferDetailsActivity.class);
                OffersObj offer = offers.get(position);

                intent.putExtra(IE_TITLE, offer.getName());
                intent.putExtra(IE_IMG_URL, offer.getImage());
                intent.putExtra(IE_DESCRIPTION, offer.getDescription());

                startActivity(intent);
            }
        });
    }


}
