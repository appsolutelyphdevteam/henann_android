package ph.appsolutely.henann.dashboard.content.rewards;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;
import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.DashboardActivity;
import ph.appsolutely.henann.dashboard.content.rewards.adapter.RewardsListAdapter;
import ph.appsolutely.henann.dashboard.content.rewards.pojo.RewardObj;
import ph.appsolutely.henann.dashboard.content.rewards.pojo.UserObj;

import static ph.appsolutely.henann.utilities.constants.UIConstants.REWARDS_PAGE;
import static ph.appsolutely.henann.utilities.constants.UIConstants.TOOLBAR_TITLE;

/**
 * Created by Jibo on 1/30/18.
 */

public class RewardsPage extends Fragment {

    // TODO: this is temporary
    UserObj user = new UserObj(10000, "prepare.to.die@gmail.com", "https://img00.deviantart.net/865d/i/2013/058/8/7/dark_souls___solaire_of_astora_by_lucianoc-d5wgij7.jpg");
    RewardObj[] rewards = new RewardObj[] {
            new RewardObj(10, "lorem ispum dolor dolor dolor"),
            new RewardObj(20, "lorem ispum dolor dolor dolores")
    };

    TextView email;
    TextView points;
    ImageView profileImg;

    ListView rewardsList;
    RewardsListAdapter rewardsListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View v = inflater.inflate(R.layout.content_fragment_rewards, container, false);

        initialize(v);

        return v;

    }

    private void initialize(View v) {

        points = v.findViewById(R.id.points);
        email = v.findViewById(R.id.profile_email);
        profileImg = v.findViewById(R.id.profile_img);
        rewardsList = v.findViewById(R.id.rewards_list);

        setUser();
        setRewards();

        // set title
        ((DashboardActivity) getActivity())
                .setToolbarTitle(TOOLBAR_TITLE[REWARDS_PAGE]);


    }

    /**
     * USER
     *
     */

    private void setUser() {
        email.setText(user.getEmail());
        points.setText(user.getPointsString());

        Picasso.with(getActivity())
                .load(user.getImgUrl())
                .transform( new CropCircleTransformation() )
                .resize(60, 60)
                .onlyScaleDown()
                .centerCrop()
                .into(profileImg);

    }


    /**
     * REWARDS
     *
     */
    private void setRewards() {
        rewardsListAdapter = new RewardsListAdapter(getActivity(), rewards);
        rewardsList.setAdapter(rewardsListAdapter);
    }
}
