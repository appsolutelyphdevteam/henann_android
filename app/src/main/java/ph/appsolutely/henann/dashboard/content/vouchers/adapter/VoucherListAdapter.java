package ph.appsolutely.henann.dashboard.content.vouchers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.content.news.pojo.NewsObj;
import ph.appsolutely.henann.dashboard.content.vouchers.pojo.VoucherObj;

/**
 * Created by Jibo on 1/31/18.
 */

public class VoucherListAdapter extends BaseAdapter {

    private Context mContext;
    private VoucherObj[] vouchers;

    public VoucherListAdapter(Context mContext, VoucherObj[] vouchers) {
        this.mContext = mContext;
        this.vouchers = vouchers;
    }

    @Override
    public int getCount() {
        return vouchers.length;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return vouchers[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(R.layout.view_li_with_img_m, null);


        ImageView image = convertView.findViewById(R.id.image);
        TextView title = convertView.findViewById(R.id.title);

        int imgWidth = parent.getWidth();
        int imgHeight = (imgWidth * 40) / 100; // getting 40% of parent's width

        Picasso.with(mContext)
                .load(vouchers[position].getImgUrl())
                .transform( new RoundedCornersTransformation(12, 0, RoundedCornersTransformation.CornerType.TOP) )
                .resize(imgWidth, imgHeight)
                .onlyScaleDown()
                .centerCrop()
                .into(image);

        title.setText(vouchers[position].getName());

        return convertView;
    }
}
