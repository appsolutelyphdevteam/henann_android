package ph.appsolutely.henann.dashboard.content.rewards.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.content.rewards.pojo.RewardObj;

/**
 * Created by Jibo on 2/1/18.
 */

public class RewardsListAdapter extends BaseAdapter {

    private Context mContext;
    private RewardObj[] rewards;

    public RewardsListAdapter(Context mContext, RewardObj[] rewards) {
        this.mContext = mContext;
        this.rewards = rewards;
    }

    @Override
    public int getCount() {
        return rewards.length;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return rewards[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(R.layout.view_li_rewards, null);

        TextView points = convertView.findViewById(R.id.points);
        TextView description = convertView.findViewById(R.id.description);

        points.setText(rewards[position].getStringPoints());
        description.setText(rewards[position].getDescription());

        return convertView;
    }

}
