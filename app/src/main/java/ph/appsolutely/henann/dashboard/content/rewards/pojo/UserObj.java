package ph.appsolutely.henann.dashboard.content.rewards.pojo;

/**
 * Created by Jibo on 2/1/18.
 */

public class UserObj {

    private String fname;
    private String lname;
    private String email;
    private String imgUrl;
    private int points ;

    // TODO: this is only temporary
    public UserObj(int points, String email, String imgUrl) {
        this.points = points;
        this.email = email;
        this.imgUrl = imgUrl;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getPointsString() {
        return points+"";
    }

    public String getEmail() {
        return email;
    }

    public String getImgUrl() {
        return imgUrl;
    }
}
