package ph.appsolutely.henann.dashboard.content.offers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.content.offers.pojo.OffersObj;

/**
 * Created by Jibo on 2/1/18.
 */

public class OffersListAdapter extends BaseAdapter {

    private Context mContext;
    private List<OffersObj> offers;

    public OffersListAdapter(Context mContext, List<OffersObj> offers) {
        this.mContext = mContext;
        this.offers = offers;
    }

    @Override
    public int getCount() {
        return offers.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return offers.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(R.layout.view_li_with_img_l, null);


        ImageView image = convertView.findViewById(R.id.image);
        TextView title = convertView.findViewById(R.id.title);

        int imgWidth = parent.getWidth();
        int imgHeight = (imgWidth * 50) / 100; // getting 50% of parent's width

        Picasso.with(mContext)
                .load(offers.get(position).getImage())
                .transform( new RoundedCornersTransformation(12, 0, RoundedCornersTransformation.CornerType.TOP) )
                .resize(imgWidth, imgHeight)
                .onlyScaleDown()
                .centerCrop()
                .into(image);

        title.setText(offers.get(position).getName());

        return convertView;
    }
}
