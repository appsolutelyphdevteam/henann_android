package ph.appsolutely.henann.dashboard.tab_navigation.pojo;

/**
 * Created by Jibo on 1/29/18.
 */

public class TabLink {

    /**
     * tabIndex = unused
     * imgResource = tab default icon
     * imgResourceActive = tab active icon
     */

    private int tabIndex;
    private int imgResource;
    private int imgResourceActive;
    private String tabTitle;
    private boolean isActive = false;

    public TabLink(int imgResource, int imgResourceActive, String tabTitle) {

        setImgResource(imgResource);
        setImgResourceActive(imgResourceActive);
        setTabTitle(tabTitle);

    }

    public void setTabIndex(int tabIndex) {
        this.tabIndex = tabIndex;
    }

    public int getTabIndex() {
        return this.tabIndex;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }

    public int getImgResource() {
        return this.imgResource;
    }

    public void setImgResourceActive(int imgResourceActive) {
        this.imgResourceActive = imgResourceActive;
    }

    public int getImgResourceActive() {
        return this.imgResourceActive;
    }

    public void setTabTitle(String tabTitle) {
        this.tabTitle = tabTitle;
    }

    public String getTabTitle() {
        return this.tabTitle;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

}
