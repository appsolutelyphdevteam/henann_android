package ph.appsolutely.henann.dashboard.content.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.content.news.pojo.NewsObj;

/**
 * Created by Jibo on 1/31/18.
 */

public class NewsListAdapter extends BaseAdapter {

    private Context mContext;
    private NewsObj[] news;

    public NewsListAdapter(Context mContext, NewsObj[] news) {
        this.mContext = mContext;
        this.news = news;
    }

    @Override
    public int getCount() {
        return news.length;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return news[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(R.layout.view_li_with_img_l, null);


        ImageView image = convertView.findViewById(R.id.image);
        TextView title = convertView.findViewById(R.id.title);

        int imgWidth = parent.getWidth();
        int imgHeight = (imgWidth * 50) / 100; // getting 50% of parent's width

        Picasso.with(mContext)
                .load(news[position].getImgUrl())
                .transform( new RoundedCornersTransformation(12, 0, RoundedCornersTransformation.CornerType.TOP) )
                .resize(imgWidth, imgHeight)
                .onlyScaleDown()
                .centerCrop()
                .into(image);

        title.setText(news[position].getTitle());

        return convertView;
    }
}
