package ph.appsolutely.henann.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.content_activities.about.AboutActivity;
import ph.appsolutely.henann.content_activities.faqs.FaqsActivity;
import ph.appsolutely.henann.content_activities.locations.LocationsActivity;
import ph.appsolutely.henann.content_activities.terms.TermsActivity;
import ph.appsolutely.henann.dashboard.adapters.DrawerNavAdapter;
import ph.appsolutely.henann.dashboard.content.mycode.MyCodePage;
import ph.appsolutely.henann.dashboard.content.news.NewsPage;
import ph.appsolutely.henann.dashboard.content.offers.OffersPage;
import ph.appsolutely.henann.dashboard.content.rewards.RewardsPage;
import ph.appsolutely.henann.dashboard.content.vouchers.VouchersPage;
import ph.appsolutely.henann.dashboard.tab_navigation.adapter.TabNavAdapter;
import ph.appsolutely.henann.dashboard.tab_navigation.listener.RecyclerItemClickListener;
import ph.appsolutely.henann.dashboard.tab_navigation.pojo.TabLink;

import static ph.appsolutely.henann.utilities.constants.UIConstants.NAVIGATION_LINKS;

/**
 * Created by Jibo on 1/22/18.
 */

public class DashboardActivity extends AppCompatActivity {

    public static final int DEFAULT_PAGE = 0;

    Toolbar toolbar;
    TextView toolbarTitle;

    ListView navList;
    LinearLayout drawer;
    DrawerLayout drawerLayout;
    DrawerNavAdapter navAdapter;

    TabLink tabLinks[];
    RecyclerView tabRecycler;
    TabNavAdapter tabRecyclerAdapter;
    LinearLayoutManager tabLayoutManager;

    Fragment[] pages;
    FragmentManager fm;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.activity_dashboard);

        initialize();
    }

    private void initialize() {
        drawer = findViewById(R.id.drawer);
        navList = findViewById(R.id.drawer_navigation);
        drawerLayout = findViewById(R.id.drawer_layout);

        setTabBar();
        setToolbar();
        setPages();
        setDrawerNavigation();

    }

    /**
     * PAGES
     *
     */

    private void setPages() {
        pages = new Fragment[] {
                new NewsPage(),
                new RewardsPage(),
                new MyCodePage(),
                new VouchersPage(),
                new OffersPage()
        };

        fm = getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        ft.add(R.id.content_holder, pages[DEFAULT_PAGE]);
        ft.commit();

        tabRecyclerAdapter.setActiveTab(DEFAULT_PAGE);

    }

    /**
     * DRAWER NAVIGATION
     *
     */

    private void setDrawerNavigation() {

        navAdapter = new DrawerNavAdapter(this, NAVIGATION_LINKS);

        navList.setAdapter(navAdapter);

        navList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: // location
                        startActivity(new Intent(DashboardActivity.this, LocationsActivity.class));
                        break;
                    case 1: // about
                        startActivity(new Intent(DashboardActivity.this, AboutActivity.class));
                        break;
                    case 2: // faqs
                        startActivity(new Intent(DashboardActivity.this, FaqsActivity.class));
                        break;
                    case 3: // terms
                        startActivity(new Intent(DashboardActivity.this, TermsActivity.class));
                        break;
                    case 4: // log out
                        break;
                    default:
                        break;
                }
            }
        });

    }

    /**
     * TOOL BAR
     *
     */

    private void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.text_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(drawer);
            }
        });


    }

    public void setToolbarTitle(String title) {
        if (title.isEmpty()) {

        } else {
            toolbarTitle.setText(title);
        }
    }

    /**
     * TAB BAR
     *
     */

    private void setTabBar() {
        tabLinks = new TabLink[] {
                new TabLink(R.drawable.ic_tab_news, R.drawable.ic_tab_news_active, getResources().getString(R.string.tab_0)),
                new TabLink(R.drawable.ic_tab_reward, R.drawable.ic_tab_reward_active, getResources().getString(R.string.tab_1)),
                new TabLink(R.drawable.ic_tab_code, R.drawable.ic_tab_code_active, getResources().getString(R.string.tab_2)),
                new TabLink(R.drawable.ic_tab_vouchers, R.drawable.ic_tab_vouchers_active, getResources().getString(R.string.tab_3)),
                new TabLink(R.drawable.ic_tab_offers, R.drawable.ic_tab_offers_active, getResources().getString(R.string.tab_4))
        };

        tabRecycler = findViewById(R.id.tab_list);

        tabLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        tabRecyclerAdapter = new TabNavAdapter(this, tabLinks);

        tabRecycler.setLayoutManager(tabLayoutManager);
        tabRecycler.setAdapter(tabRecyclerAdapter);

        tabRecycler.addOnItemTouchListener(new RecyclerItemClickListener(this,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        if (position == tabRecyclerAdapter.getPosition()) return;

                        FragmentTransaction ft = fm.beginTransaction();

                        ft.replace(R.id.content_holder, pages[position]);
                        ft.commit();

                        tabRecyclerAdapter.setActiveTab(position);

                    }
                }));
    }

}
