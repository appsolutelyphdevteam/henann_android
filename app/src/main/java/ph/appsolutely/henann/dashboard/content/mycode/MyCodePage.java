package ph.appsolutely.henann.dashboard.content.mycode;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.DashboardActivity;

import static ph.appsolutely.henann.utilities.constants.UIConstants.CODE_PAGE;
import static ph.appsolutely.henann.utilities.constants.UIConstants.TOOLBAR_TITLE;

/**
 * Created by Jibo on 1/30/18.
 */

public class MyCodePage extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View v = inflater.inflate(R.layout.content_fragment_code, container, false);

        initialize(v);

        return v;

    }

    private void initialize(View v) {

        // set title
        ((DashboardActivity) getActivity())
                .setToolbarTitle(TOOLBAR_TITLE[CODE_PAGE]);



    }
}
