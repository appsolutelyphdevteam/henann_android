package ph.appsolutely.henann.dashboard.content.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.DashboardActivity;
import ph.appsolutely.henann.dashboard.content.news.adapter.NewsListAdapter;
import ph.appsolutely.henann.dashboard.content.news.pojo.NewsObj;
import ph.appsolutely.henann.utilities.WebServices;

import static ph.appsolutely.henann.utilities.constants.ApiConst.FETCH_TABLE;
import static ph.appsolutely.henann.utilities.constants.ApiConst.POST;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_DESCRIPTION;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_IMG_URL;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_TITLE;
import static ph.appsolutely.henann.utilities.constants.UIConstants.NEWS_PAGE;
import static ph.appsolutely.henann.utilities.constants.UIConstants.TOOLBAR_TITLE;

/**
 * Created by Jibo on 1/30/18.
 */

public class NewsPage
        extends Fragment
        implements
            Response.Listener<String>,
            Response.ErrorListener {

    // TODO: this is temporary
    NewsObj[] news = new NewsObj[] {
            new NewsObj("test1", "https://img00.deviantart.net/6325/i/2016/109/7/9/dark_souls_frame_by_parkurtommo-d9zgof8.png", "test1"),
            new NewsObj("test2", "https://img00.deviantart.net/c47c/i/2016/129/1/d/dove_by_parkurtommo-da1wzir.png", "test2"),
            new NewsObj("test3", "https://img00.deviantart.net/854e/i/2016/132/5/a/reunited_by_parkurtommo-da28tva.png", "test3"),
    };

    ListView newsList;
    NewsListAdapter newsListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View v = inflater.inflate(R.layout.content_fragment_news, container, false);

        initialize(v);

        return v;

    }

    private void initialize(View v) {

        newsList = v.findViewById(R.id.list);

        // set title
        ((DashboardActivity) getActivity())
                .setToolbarTitle(TOOLBAR_TITLE[NEWS_PAGE]);

        setNewsList();

        WebServices.fetchJson(getContext(), FETCH_TABLE[POST], this);

    }

    private void setNewsList() {
        newsListAdapter = new NewsListAdapter(getActivity(), news);
        newsList.setAdapter(newsListAdapter);
        newsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);

                intent.putExtra(IE_TITLE, news[position].getTitle());
                intent.putExtra(IE_IMG_URL, news[position].getImgUrl());
                intent.putExtra(IE_DESCRIPTION, news[position].getDescription());

                startActivity(intent);
            }
        });
    }

    @Override
    public void onResponse(String response) {
        Log.i("test", response);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }


}
