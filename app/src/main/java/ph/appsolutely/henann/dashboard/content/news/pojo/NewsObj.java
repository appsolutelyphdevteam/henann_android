package ph.appsolutely.henann.dashboard.content.news.pojo;

/**
 * Created by Jibo on 1/31/18.
 */

public class NewsObj {

    private String title;
    private String imgUrl;
    private String description;

    // TODO: this is temporary
    public NewsObj(String title, String imgUrl, String description) {
        this.title = title;
        this.imgUrl = imgUrl;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getDescription() {
        return description;
    }



}
