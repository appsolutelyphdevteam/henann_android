package ph.appsolutely.henann.dashboard.content.vouchers.pojo;

/**
 * Created by Jibo on 1/31/18.
 */

public class VoucherObj {

    private String name;
    private String imgUrl;
    private String description;

    // TODO: this is temporary
    public VoucherObj(String name, String imgUrl, String description) {
        this.name = name;
        this.imgUrl = imgUrl;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getDescription() {
        return description;
    }


}
