package ph.appsolutely.henann.dashboard.content.rewards.pojo;

/**
 * Created by Jibo on 2/1/18.
 */

public class RewardObj {

    private int points;
    private String description;

    // TODO: this is temporary
    public RewardObj(int points, String description) {
        this.points = points;
        this.description = description;
    }

    public String getStringPoints() {
        return points+"";
    }

    public String getDescription() {
        return description;
    }
}
