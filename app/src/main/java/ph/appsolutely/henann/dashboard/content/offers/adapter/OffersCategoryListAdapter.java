package ph.appsolutely.henann.dashboard.content.offers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.content.offers.pojo.OffersCategoryObj;

/**
 * Created by Jibo on 2/1/18.
 */

public class OffersCategoryListAdapter extends BaseAdapter {

    private Context mContext;
    private OffersCategoryObj[] offersCategories;

    public OffersCategoryListAdapter(Context mContext, OffersCategoryObj[] offersCategories) {
        this.mContext = mContext;
        this.offersCategories = offersCategories;
    }

    @Override
    public int getCount() {
        return offersCategories.length;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return offersCategories[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(R.layout.view_li_offer_category, null);


        ImageView image = convertView.findViewById(R.id.image);
        TextView category = convertView.findViewById(R.id.category);

        int imgWidth = parent.getWidth();
        int imgHeight = (imgWidth * 60) / 100; // getting 50% of parent's width

        Picasso.with(mContext)
//                .load(offersCategories[position].getImage())
                .load(R.drawable.bg_turquoise)
                .resize(imgWidth, imgHeight)
                .onlyScaleDown()
                .centerCrop()
                .into(image);

        category.setText(offersCategories[position].getName());

        return convertView;
    }
}
