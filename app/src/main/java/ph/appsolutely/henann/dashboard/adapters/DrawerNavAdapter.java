package ph.appsolutely.henann.dashboard.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import ph.appsolutely.henann.R;

/**
 * Created by Jibo on 1/26/18.
 */

public class DrawerNavAdapter extends BaseAdapter {

    Context mContext;
    String[] data;
    private static LayoutInflater inflater = null;

    public DrawerNavAdapter(Context mContext, String[] data) {
        this.mContext = mContext;
        this.data = data;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View _convertView = convertView;

        if (_convertView == null)
            _convertView = inflater.inflate(R.layout.view_drawer_nav_item, null);

        TextView text = _convertView.findViewById(R.id.text_nav_link);
        text.setText(data[position]);
        return _convertView;
    }
}
