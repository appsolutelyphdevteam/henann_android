package ph.appsolutely.henann.dashboard.content.offers;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.DashboardActivity;
import ph.appsolutely.henann.dashboard.content.offers.adapter.OffersCategoryListAdapter;
import ph.appsolutely.henann.dashboard.content.offers.pojo.OffersCategoryObj;
import ph.appsolutely.henann.dashboard.content.offers.pojo.OffersObj;
import ph.appsolutely.henann.utilities.WebServices;
import ph.appsolutely.henann.utilities.LoaderDialog;

import static ph.appsolutely.henann.utilities.constants.ApiConst.FETCH_TABLE;
import static ph.appsolutely.henann.utilities.constants.ApiConst.PRODUCT;
import static ph.appsolutely.henann.utilities.constants.ErrorMessages.CONNECTION_ERR_MSG;
import static ph.appsolutely.henann.utilities.constants.ErrorMessages.JSON_PARSE_ERR_MSG;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_TITLE;
import static ph.appsolutely.henann.utilities.constants.KeyConst.DESCRIPTION;
import static ph.appsolutely.henann.utilities.constants.UIConstants.OFFERS_PAGE;
import static ph.appsolutely.henann.utilities.constants.UIConstants.TOOLBAR_TITLE;

/**
 * Created by Jibo on 1/30/18.
 */

public class OffersPage
        extends Fragment
        implements
            Response.Listener<String>,
            Response.ErrorListener {

    public static final String PARC_OFFERS_KEY = "parcelable_offers";

    private static final String BRAND_KEY = "brand";
    private static final String PRODUCTS_KEY = "products";

    ListView categoryList;
    OffersCategoryListAdapter categoryListAdapter;

    LoaderDialog loader;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View v = inflater.inflate(R.layout.content_fragment_offers, container, false);

        initialize(v);

        return v;

    }

    private void initialize(View v) {

        categoryList = v.findViewById(R.id.list);

        loader = new LoaderDialog(getActivity());

//        setCategoryList();

        // set title
        ((DashboardActivity) getActivity())
                .setToolbarTitle(TOOLBAR_TITLE[OFFERS_PAGE]);

        WebServices.fetchJson(getContext(), FETCH_TABLE[PRODUCT], this);
        loader.show();

    }

    private void setCategoryList(final OffersCategoryObj[] categories, final OffersObj[] offers) {

        categoryListAdapter = new OffersCategoryListAdapter(getActivity(), categories);
        categoryList.setAdapter(categoryListAdapter);

        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), OffersListActivity.class);
                ArrayList<Parcelable> offersListData = new ArrayList<>();

                for (OffersObj offer : offers) {
                    if (categories[position].getBrandID().equals(offer.getBrandID())) {
                        offersListData.add(offer);
                    }
                }

                intent.putExtra(IE_TITLE, categories[position].getName());
                intent.putParcelableArrayListExtra(PARC_OFFERS_KEY, offersListData);

                startActivity(intent);
            }
        });

    }

    @Override
    public void onResponse(String response) {
        loader.hide();
        try {
            JSONObject jsonResponse = new JSONObject(response);

            if ( jsonResponse.getString("response").equals("Success") ) {
                JSONArray jsonArrayData = jsonResponse.getJSONArray("data");

                if (jsonArrayData.length() != 0) {

                    JSONArray jsonCategories = jsonArrayData.getJSONObject(0).getJSONArray(BRAND_KEY);
                    JSONArray jsonOffers = jsonArrayData.getJSONObject(0).getJSONArray(PRODUCTS_KEY);

                    OffersCategoryObj[] categories = new Gson().fromJson(jsonCategories.toString(), OffersCategoryObj[].class);
                    OffersObj[] offers = new Gson().fromJson(jsonOffers.toString(), OffersObj[].class);

                    setCategoryList(categories, offers);

                } else {
                    // TODO: add empty state
                }

            } else {
                Toast.makeText(getActivity(), jsonResponse.getString(DESCRIPTION), Toast.LENGTH_SHORT).show();
            }

        } catch(JSONException e) {
            Toast.makeText(getActivity(), JSON_PARSE_ERR_MSG, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        loader.hide();
        Toast.makeText(getActivity(), CONNECTION_ERR_MSG, Toast.LENGTH_SHORT).show();
        error.printStackTrace();
    }
}
