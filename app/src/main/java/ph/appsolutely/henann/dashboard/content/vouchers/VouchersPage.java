package ph.appsolutely.henann.dashboard.content.vouchers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import ph.appsolutely.henann.R;
import ph.appsolutely.henann.dashboard.DashboardActivity;
import ph.appsolutely.henann.dashboard.content.vouchers.adapter.VoucherListAdapter;
import ph.appsolutely.henann.dashboard.content.vouchers.pojo.VoucherObj;

import static ph.appsolutely.henann.utilities.constants.UIConstants.TOOLBAR_TITLE;
import static ph.appsolutely.henann.utilities.constants.UIConstants.VOUCHERS_PAGE;

/**
 * Created by Jibo on 1/30/18.
 */

public class VouchersPage extends Fragment {

    VoucherObj[] vouchers = new VoucherObj[] {
            new VoucherObj("test1", "https://img00.deviantart.net/6325/i/2016/109/7/9/dark_souls_frame_by_parkurtommo-d9zgof8.png", "test1"),
            new VoucherObj("test2", "https://img00.deviantart.net/c47c/i/2016/129/1/d/dove_by_parkurtommo-da1wzir.png", "test2"),
            new VoucherObj("test3", "https://img00.deviantart.net/854e/i/2016/132/5/a/reunited_by_parkurtommo-da28tva.png", "test3"),
    };

    ListView voucherList;
    VoucherListAdapter voucherListAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View v = inflater.inflate(R.layout.content_fragment_vouchers, container, false);

        initialize(v);

        return v;

    }

    private void initialize(View v) {

        voucherList = v.findViewById(R.id.list);

        setVoucherList();

        // set title
        ((DashboardActivity) getActivity())
                .setToolbarTitle(TOOLBAR_TITLE[VOUCHERS_PAGE]);


    }

    private void setVoucherList() {
        voucherListAdapter = new VoucherListAdapter(getActivity(), vouchers);

        voucherList.setAdapter(voucherListAdapter);

    }
}
