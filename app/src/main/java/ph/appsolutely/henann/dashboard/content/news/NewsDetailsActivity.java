package ph.appsolutely.henann.dashboard.content.news;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ph.appsolutely.henann.R;

import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_DESCRIPTION;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_IMG_URL;
import static ph.appsolutely.henann.utilities.constants.KeyConst.IE_TITLE;

/**
 * Created by Jibo on 2/6/18.
 */

public class NewsDetailsActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle;

    ImageView image;
    TextView title;
    TextView description;

    Bundle params;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.content_activity_details_share);

        initialize();

    }

    private void initialize() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.text_title);
        image = findViewById(R.id.img_display);
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);

        params = getIntent().getExtras();

        setToolbar();
        setContent();

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbarTitle.setText(params.getString(IE_TITLE));
    }

    private void setContent() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = (displayMetrics.widthPixels * 50) / 100;
        int width = displayMetrics.widthPixels;

        Picasso.with(this)
                .load(params.getString(IE_IMG_URL))
                .resize(width, height)
                .onlyScaleDown()
                .centerCrop()
                .into(image);

        title.setText(params.getString(IE_TITLE));
        description.setText(params.getString(IE_DESCRIPTION));
    }
}
